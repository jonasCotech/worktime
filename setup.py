from setuptools import setup
setup(
  name="worktime",
  version="0.1.0",
  entry_points={
    "console_scripts": [
      "worktime=worktime:main"
    ]
  },
  packages=["worktime"]
)

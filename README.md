# worktime

worktime is a command line tool that extracts the working hours from an .ics file,
substracts a break (30 minutes), sums it up for each week and displays the results.